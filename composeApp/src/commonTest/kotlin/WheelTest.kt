import net.hackergarten.Candidate
import net.hackergarten.NoMoreCandidatesException
import net.hackergarten.Wheel
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class WheelTest {
    @Test
    fun spinTheWheelWithOneCandidate() {
        val candidate = Candidate("bibi", emptyList())
        val wheel = Wheel(listOf(candidate))


        assertEquals(candidate, wheel.spinTheWheel())
    }

    @Test
    fun spinningAWheelTwoTimesWithOneCandidate() {
        val candidate = Candidate("bibi", emptyList())
        val wheel = Wheel(listOf(candidate))

        wheel.spinTheWheel()

        assertFailsWith(NoMoreCandidatesException::class) { wheel.spinTheWheel() }
    }

}
