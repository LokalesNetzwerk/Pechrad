package net.hackergarten

import kotlinx.datetime.LocalDate

data class Vacation(val beginDate: LocalDate, val endDate: LocalDate) {
    fun contains(day: LocalDate): Boolean {
        return day in beginDate..endDate
    }
}