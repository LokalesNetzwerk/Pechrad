package net.hackergarten

import kotlinx.datetime.LocalDate

class Wheel(totalCandidates: List<Candidate>) {

    private var currentCandidates: MutableList<Candidate> = totalCandidates.toMutableList()
    fun spinTheWheel(): Candidate {
        if (currentCandidates.isEmpty()) throw NoMoreCandidatesException()

        val today = LocalDate.now()
        val selectedCandidate = currentCandidates
            .filter { candidate -> candidate.isAvailableFor(today) }
            .random()
        currentCandidates.remove(selectedCandidate)
        return selectedCandidate
    }
}

