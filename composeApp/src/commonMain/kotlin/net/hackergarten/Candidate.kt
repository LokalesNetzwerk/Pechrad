package net.hackergarten

import kotlinx.datetime.LocalDate

data class Candidate(val name: String, val vacations: List<Vacation>) {
    fun isAvailableFor(day: LocalDate): Boolean {
        return vacations.none { vacation -> vacation.contains(day) }
    }
}