package net.hackergarten

interface CandidateImporter {
    fun read(): List<Candidate>
}