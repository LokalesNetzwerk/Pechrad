import androidx.compose.ui.window.ComposeUIViewController
import net.hackergarten.App

fun MainViewController() = ComposeUIViewController { App() }
