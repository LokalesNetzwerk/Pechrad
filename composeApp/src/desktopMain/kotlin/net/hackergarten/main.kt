package net.hackergarten

import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.runtime.Composable
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import net.hackergarten.App

fun main() = application {
    Window(onCloseRequest = ::exitApplication, title = "pechrad") {
        App()
    }
}

@Preview
@Composable
fun AppDesktopPreview() {
    App()
}