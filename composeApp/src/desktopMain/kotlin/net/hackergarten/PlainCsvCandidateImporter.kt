package net.hackergarten

import com.github.doyaaaaaken.kotlincsv.dsl.context.InsufficientFieldsRowBehaviour
import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import kotlinx.datetime.LocalDate
import java.io.File

class PlainCsvCandidateImporter(private val file: File) : CandidateImporter {


    override fun read(): List<Candidate> {
        val customReader = csvReader{
            delimiter=';'
            insufficientFieldsRowBehaviour=InsufficientFieldsRowBehaviour.EMPTY_STRING
        }
        val csvValues = customReader.readAll(file)
        val candidates = csvValues.map{ row ->
            val name = row[0]
            val vacations =  if(row[1].isNotBlank()) row[1].split(",").map{vacationString ->
                val dateStrings = vacationString.split("/")
                val beginDate = LocalDate.parse(dateStrings[0])
                val endDate = LocalDate.parse(dateStrings[1])
                Vacation(beginDate, endDate)
            }.toList()  else emptyList()
            Candidate(name, vacations)
        }.toList()
        return candidates
    }
}