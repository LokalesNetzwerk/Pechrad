package net.hackergarten

import java.io.File

actual fun readCandidates(): List<Candidate> {
    val file = File("candidates.txt")
    return PlainCsvCandidateImporter(file).read()
}